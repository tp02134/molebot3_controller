Molebot(3rd) Controller
===========
This is a Molebot controller with UI using Arduino and ROS

    roscore
    rosrun molebot_controller molebot_control
    rosrun rosserial_python serial_node.py _port:=/dev/ttyACM0 (your port) 
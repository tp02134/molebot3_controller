#include <ros.h>
#include <ros/time.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Char.h>
#include <std_msgs/String.h>
#include <Servo.h>

Servo left_arm;     //22
Servo right_arm;    //23
Servo left_linear;  //24
Servo right_linear; //25
Servo left_waist;   //26
Servo right_waist;  //27
Servo body;         //40

int len_body = 90;
int len_arm = 40;
int len_linear = 30;
int len_waist = 0;
int len_waist_left = 40;
int len_waist_right = 40;

int preSignal = LOW;
int curSignal = LOW;
int encoder = 0;
byte b;

std_msgs::Int32 enc_msg;

ros::NodeHandle nh;
ros::Publisher pub_enc("encoder", &enc_msg);

void callback_key(const std_msgs::String &key_msg){
  String msg = key_msg.data;
  if(msg == "armOpen")
  {
    len_arm = 40;
  }
  else if(msg == "armParallel")
  {
    len_arm = 65;
  }
  else if(msg == "armClose")
  {
    len_arm = 90;
  }
  else if(msg == "armForward")
  {
    len_body = 180;
  }
  else if(msg == "armStop")
  {
    len_body = 90;
  }
  else if(msg == "armBackward")
  {
    len_body = 0;
  }
  
  else if(msg == "drillForward")
  {
    len_linear = 85;
  }
  else if(msg == "drillBackward")
  {
    len_linear = 30;
  }

  else if(msg == "drillRotate")
  {
    digitalWrite(28, 1);      
    digitalWrite(30, HIGH);
    digitalWrite(32, LOW);   
  }
  else if(msg == "drillStop")
  {
    digitalWrite(28, 0);      
    digitalWrite(30, HIGH);
    digitalWrite(32, LOW);
  }
  
  else if(msg == "moveForward")
  {
    digitalWrite(29, 1); // 0~255      
    digitalWrite(31, HIGH);
    digitalWrite(33, LOW);

    digitalWrite(34, 1);    
    digitalWrite(36, HIGH);
    digitalWrite(38, LOW);

    digitalWrite(35, 1);      
    digitalWrite(37, HIGH);
    digitalWrite(39, LOW);  
  }
  else if(msg == "moveStop")
  {
    digitalWrite(29, 0);      
    digitalWrite(31, HIGH);
    digitalWrite(33, LOW);

    digitalWrite(34, 0);      
    digitalWrite(36, HIGH);
    digitalWrite(38, LOW);

    digitalWrite(35, 0);      
    digitalWrite(37, HIGH);
    digitalWrite(39, LOW);  
  }
  else if(msg == "moveBackward")
  {
    digitalWrite(29, 1);      
    digitalWrite(31, LOW);
    digitalWrite(33, HIGH);

    digitalWrite(34, 1);      
    digitalWrite(36, LOW);
    digitalWrite(38, HIGH);

    digitalWrite(35, 1);      
    digitalWrite(37, LOW);
    digitalWrite(39, HIGH);   
  }

  else if(msg == "turnLeft")
  { 
    len_waist_left -= 10;
    len_waist_right += 10;
  }         
  else if(msg == "Straight")
  {
    len_waist_left = 40;
    len_waist_right = 40;
  }
  else if(msg == "turnRight")
  {
    len_waist_left += 10;
    len_waist_right -= 10;
  }
}

ros::Subscriber<std_msgs::String> sub_key("key", &callback_key);

void setup() {
  // put your setup code here, to run once:

  nh.initNode();
  nh.advertise(pub_enc);
  nh.subscribe(sub_key);

  /*
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);

  pinMode(9, INPUT_PULLUP);
  pinMode(10, INPUT_PULLUP);
  */
  
  //28
  pinMode(30, OUTPUT);
  pinMode(32, OUTPUT);

  //29
  pinMode(31, OUTPUT);
  pinMode(33, OUTPUT);

  //34
  pinMode(36, OUTPUT);
  pinMode(38, OUTPUT);

  //35
  pinMode(37, OUTPUT);
  pinMode(39, OUTPUT);

  left_arm.attach(22);
  right_arm.attach(23);
  left_linear.attach(24);
  right_linear.attach(25);
//  left_waist.attach(26);
//  right_waist.attach(27);
  body.attach(40);

  left_linear.write(30);
  right_linear.write(30);
  left_arm.write(0);
  right_arm.write(0);
  left_waist.write(len_waist_left);
  right_waist.write(len_waist_right);
  
}

void loop() {
  // put your main code here, to run repeatedly:

  left_arm.write(len_arm);
  right_arm.write(len_arm);
  left_waist.write(len_waist_left);
  right_waist.write(len_waist_right);
  left_linear.write(len_linear);
  right_linear.write(len_linear);
  body.write(len_body);
  delay(10); 

  curSignal = digitalRead(9);
  if((preSignal == LOW) && (curSignal == HIGH))
  {
    if(digitalRead(10) == HIGH)
    {
      encoder--;
    } else{
      encoder++;
    }
  }
  preSignal = curSignal;
  
  enc_msg.data = encoder;
  pub_enc.publish(&enc_msg);

  nh.spinOnce();

//  Serial.println(preSignal);
//  Serial.println(curSignal);
}

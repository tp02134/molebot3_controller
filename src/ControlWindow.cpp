#include "../include/ControlWindow.h"

namespace server{
ControlWindow::ControlWindow(int argc, char **argv, QWidget *parent)
    : QWidget(parent),
      m_RobotThread(argc, argv)
{
    /** Button **/
    p_upButton = new QPushButton();
    p_downButton = new QPushButton();
    p_leftButton = new QPushButton();
    p_rightButton = new QPushButton();

    p_stopButton = new QPushButton(tr("&Stop"));
    p_quitButton = new QPushButton(tr("&Quit"));
    p_armButton1 = new QPushButton(tr("&Open"));
    p_armButton2 = new QPushButton(tr("&Parallel"));
    p_armButton3 = new QPushButton(tr("&Close"));
    p_armButton4 = new QPushButton();
    p_armButton5 = new QPushButton(tr("Stop"));
    p_armButton6 = new QPushButton();
    p_drillButton1 = new QPushButton(tr("&Expand"));
    p_drillButton2 = new QPushButton(tr("&Rotate"));

    QPalette palette;
    palette = p_armButton1->palette();
    palette.setColor(QPalette::Button,QColor(255,255,255));
    p_armButton1->setAutoFillBackground(true);
    p_armButton1->setPalette(palette);
    p_armButton1->setFixedSize(QSize(80,80));

    palette = p_armButton2->palette();
    palette.setColor(QPalette::Button,QColor(255,255,255));
    p_armButton2->setAutoFillBackground(true);
    p_armButton2->setPalette(palette);
    p_armButton2->setFixedSize(QSize(80,80));

    palette = p_armButton3->palette();
    palette.setColor(QPalette::Button,QColor(255,255,255));
    p_armButton3->setAutoFillBackground(true);
    p_armButton3->setPalette(palette);
    p_armButton3->setFixedSize(QSize(80,80));

    palette = p_armButton4->palette();
    palette.setColor(QPalette::Button,QColor(255,255,255));
    p_armButton4->setAutoFillBackground(true);
    p_armButton4->setFlat(true);
    p_armButton4->setPalette(palette);
    p_armButton4->setIcon(QIcon(":/images/up.xpm"));
    p_armButton4->setIconSize(QSize(80,80));

    palette = p_armButton5->palette();
    palette.setColor(QPalette::Button,QColor(255,255,255));
    p_armButton5->setAutoFillBackground(true);
    p_armButton5->setFlat(true);
    p_armButton5->setPalette(palette);
    p_armButton5->setFixedSize(QSize(80,80));

    palette = p_armButton6->palette();
    palette.setColor(QPalette::Button,QColor(255,255,255));
    p_armButton6->setAutoFillBackground(true);
    p_armButton6->setFlat(true);
    p_armButton6->setPalette(palette);
    p_armButton6->setIcon(QIcon(":/images/down.xpm"));
    p_armButton6->setIconSize(QSize(80,80));

    palette = p_drillButton1->palette();
    palette.setColor(QPalette::Button,QColor(255,255,255));
    p_drillButton1->setAutoFillBackground(true);
    p_drillButton1->setPalette(palette);
    p_drillButton1->setFixedSize(QSize(120,120));
    p_drillButton1->setCheckable(true);

    palette = p_drillButton2->palette();
    palette.setColor(QPalette::Button,QColor(255,255,255));
    p_drillButton2->setAutoFillBackground(true);
    p_drillButton2->setPalette(palette);
    p_drillButton2->setFixedSize(QSize(120,120));
    p_drillButton2->setCheckable(true);

    p_rightButton->palette();
    palette.setColor(QPalette::Button,QColor(255,255,255));
    p_rightButton->setAutoFillBackground(true);
    p_rightButton->setFlat(true);
    p_rightButton->setPalette(palette);
    p_rightButton->setIcon(QIcon(":/images/right.xpm"));
    p_rightButton->setIconSize(QSize(60, 60));

    palette = p_leftButton->palette();
    palette.setColor(QPalette::Button,QColor(255,255,255));
    p_leftButton->setAutoFillBackground(true);
    p_leftButton->setFlat(true);
    p_leftButton->setPalette(palette);
    p_leftButton->setIcon(QIcon(":/images/left.xpm"));
    p_leftButton->setIconSize(QSize(60, 60));

    palette = p_upButton->palette();
    palette.setColor(QPalette::Button,QColor(255,255,255));
    p_upButton->setAutoFillBackground(true);
    p_upButton->setFlat(true);
    p_upButton->setPalette(palette);
    p_upButton->setIcon(QIcon(":/images/up.xpm"));
    p_upButton->setIconSize(QSize(60, 60));

    palette = p_downButton->palette();
    palette.setColor(QPalette::Button,QColor(255,255,255));
    p_downButton->setAutoFillBackground(true);
    p_downButton->setFlat(true);
    p_downButton->setPalette(palette);
    p_downButton->setIcon(QIcon(":/images/down.xpm"));
    p_downButton->setIconSize(QSize(60, 60));

    palette = p_quitButton->palette();
    palette.setColor(QPalette::Button,QColor(255,255,255));
    p_quitButton->setAutoFillBackground(true);
    p_quitButton->setPalette(palette);
    p_quitButton->setFixedSize(QSize(210, 25));

    palette = p_stopButton->palette();
    palette.setColor(QPalette::Button,QColor(255,255,255));
    p_stopButton->setAutoFillBackground(true);
    p_stopButton->setFlat(true);
    p_stopButton->setPalette(palette);
    p_stopButton->setFixedSize(QSize(60,60));

    /** Label **/
    p_nameLabel3 = new QLabel();
    p_nameLabel3->setText(tr("Drilling"));
    p_nameLabel3->setAlignment(Qt::AlignCenter);
    p_nameLabel3->setFixedSize(QSize(100,50));

    p_nameLabel1 = new QLabel();
    p_nameLabel1->setText(tr("Forelimb"));
    p_nameLabel1->setAlignment(Qt::AlignCenter);
    p_nameLabel1->setFixedSize(QSize(100,50));

    p_nameLabel2 = new QLabel();
    p_nameLabel2->setText(tr("Movement"));
    p_nameLabel2->setAlignment(Qt::AlignCenter);
    p_nameLabel2->setFixedSize(QSize(100,50));

    /** display **/
    p_display1 = new QLineEdit;
    p_display1->setText(tr("Straight"));
    p_display1->setFixedSize(QSize(100,25));
    p_display1->setAlignment(Qt::AlignHCenter);

    p_display2 = new QLineEdit;
    p_display2->setText(tr("0"));
    p_display2->setFixedSize(QSize(100,25));
    p_display2->setAlignment(Qt::AlignHCenter);

    /** line **/
    p_line1 = new QFrame;
    p_line1->setFrameShape(QFrame::HLine);
    p_line1->setLineWidth(10);

    p_line2 = new QFrame;
    p_line2->setFrameShape(QFrame::HLine);
    p_line2->setLineWidth(10);

    p_line3 = new QFrame;
    p_line3->setFrameShape(QFrame::HLine);
    p_line3->setLineWidth(10);

    p_line4 = new QFrame;
    p_line4->setFrameShape(QFrame::HLine);

    p_line5 = new QFrame;
    p_line5->setFrameShape(QFrame::HLine);

    /** drill layout **/
    drillLayout = new QVBoxLayout();
    drillLayout->setAlignment(Qt::AlignTop);

    subdrillLayout1 = new QHBoxLayout();
    subdrillLayout2 = new QHBoxLayout();
    subdrillLayout3 = new QHBoxLayout();
    subdrillLayout4 = new QHBoxLayout();
    subdrillLayout5 = new QHBoxLayout();

    subdrillLayout1->addWidget(p_nameLabel3);
    subdrillLayout2->addWidget(p_line1);
    subdrillLayout3->addWidget(p_drillButton1);
    subdrillLayout4->addWidget(p_line4);
    subdrillLayout5->addWidget(p_drillButton2);

    drillLayout->addLayout(subdrillLayout1);
    drillLayout->addLayout(subdrillLayout2);
    drillLayout->addLayout(subdrillLayout3);
    drillLayout->addLayout(subdrillLayout4);
    drillLayout->addLayout(subdrillLayout5);


    /** forelimb layout **/
    forelimbLayout = new QVBoxLayout();
    forelimbLayout->setAlignment(Qt::AlignTop);

    subforelimbLayout1 = new QHBoxLayout();
    subforelimbLayout2 = new QHBoxLayout();
    subforelimbLayout3 = new QVBoxLayout();
    subforelimbLayout4 = new QVBoxLayout();
    subforelimbLayout5 = new QHBoxLayout();

    subforelimbLayout1->addWidget(p_nameLabel1);
    subforelimbLayout2->addWidget(p_line2);
    subforelimbLayout3->addWidget(p_armButton1);
    subforelimbLayout3->addWidget(p_armButton2);
    subforelimbLayout3->addWidget(p_armButton3);
    subforelimbLayout4->addWidget(p_armButton4);
    subforelimbLayout4->addWidget(p_armButton5);
    subforelimbLayout4->addWidget(p_armButton6);

    subforelimbLayout4->setAlignment(p_armButton5, Qt::AlignHCenter);

    forelimbLayout->addLayout(subforelimbLayout1);
    forelimbLayout->addLayout(subforelimbLayout2);
    forelimbLayout->addLayout(subforelimbLayout5);
    subforelimbLayout5->addLayout(subforelimbLayout3);
    subforelimbLayout5->addLayout(subforelimbLayout4);

    /** movement layout **/
    movementLayout = new QVBoxLayout();
    movementLayout->setAlignment(Qt::AlignTop);

    submovementLayout1 = new QHBoxLayout();
    submovementLayout2 = new QHBoxLayout();
    submovementLayout3 = new QHBoxLayout();
    submovementLayout4 = new QHBoxLayout();
    submovementLayout5 = new QHBoxLayout();
    submovementLayout6 = new QHBoxLayout();
    submovementLayout7 = new QHBoxLayout();

    submovementLayout1->addWidget(p_nameLabel2);
    submovementLayout2->addWidget(p_line3);
    submovementLayout3->addWidget(p_upButton);
    submovementLayout4->addWidget(p_leftButton);
    submovementLayout4->addWidget(p_stopButton);
    submovementLayout4->addWidget(p_rightButton);
    submovementLayout5->addWidget(p_downButton);
    submovementLayout6->addWidget(p_display1);
    submovementLayout6->addWidget(p_display2);
    submovementLayout7->addWidget(p_quitButton, 6);

    movementLayout->addLayout(submovementLayout1);
    movementLayout->addLayout(submovementLayout2);
    movementLayout->addLayout(submovementLayout3);
    movementLayout->addLayout(submovementLayout4);
    movementLayout->addLayout(submovementLayout5);
    movementLayout->addLayout(submovementLayout6);
    movementLayout->addLayout(submovementLayout7);

    /** Main layout **/
    mainLayout = new QHBoxLayout();
    mainLayout->addLayout(drillLayout);
    mainLayout->addLayout(forelimbLayout);
    mainLayout->addLayout(movementLayout);
    setLayout(mainLayout);

    show();

    setWindowTitle(tr("Molebot Control Window"));

    connect(p_drillButton1, &QPushButton::toggled, this, &ControlWindow::drillExpand);
    connect(p_drillButton2, &QPushButton::toggled, this, &ControlWindow::drillRotate);

    connect(p_armButton1, &QPushButton::clicked, this, &ControlWindow::armOpen);
    connect(p_armButton2, &QPushButton::clicked, this, &ControlWindow::armParallel);
    connect(p_armButton3, &QPushButton::clicked, this, &ControlWindow::armClose);
    connect(p_armButton4, &QPushButton::clicked, this, &ControlWindow::armForward);
    connect(p_armButton5, &QPushButton::clicked, this, &ControlWindow::armStop);
    connect(p_armButton6, &QPushButton::clicked, this, &ControlWindow::armBackward);

    connect(p_upButton,    &QPushButton::clicked, this, &ControlWindow::moveForward);
    connect(p_leftButton,  &QPushButton::clicked, this, &ControlWindow::turnLeft);
    connect(p_rightButton, &QPushButton::clicked, this, &ControlWindow::turnRight);
    connect(p_downButton,  &QPushButton::clicked, this, &ControlWindow::moveBackward);
    connect(p_stopButton,  &QPushButton::clicked, this, &ControlWindow::stop);

    connect(p_quitButton,  &QPushButton::clicked, this, &ControlWindow::close);

    connect(&m_RobotThread, &RobotThread::newPose, this, &ControlWindow::updatePoseDisplay);


    m_RobotThread.init();
}//end constructor

void ControlWindow::moveForward(){m_RobotThread.moveForward();}
void ControlWindow::moveBackward(){m_RobotThread.moveBackward();}
void ControlWindow::stop(){ m_RobotThread.moveStop();}

void ControlWindow::turnLeft()
{
    num--;
    if(num > 0)
        p_display1->setText(tr("Right"));
    else if(num == 0)
        p_display1->setText(tr("Straight"));
    else
        p_display1->setText(tr("Left"));

    QString qs;
    qs.setNum(abs(num));
    p_display2->setText(qs);

    m_RobotThread.turnLeft();
}
void ControlWindow::turnRight()
{
    num++;
    if(num > 0)
        p_display1->setText(tr("Right"));
    else if(num == 0)
        p_display1->setText(tr("Straight"));
    else
        p_display1->setText(tr("Left"));

    QString qs;
    qs.setNum(abs(num));
    p_display2->setText(qs);

    m_RobotThread.turnRight();
}

void ControlWindow::armOpen(){m_RobotThread.armOpen();}
void ControlWindow::armParallel(){m_RobotThread.armParallel();}
void ControlWindow::armClose(){m_RobotThread.armClose();}
void ControlWindow::armForward(){m_RobotThread.armForward();}
void ControlWindow::armStop(){m_RobotThread.armStop();}
void ControlWindow::armBackward(){m_RobotThread.armBackward();}

void ControlWindow::close(){this->close();}

void ControlWindow::drillExpand()
{
    if(p_drillButton1->isChecked())
        m_RobotThread.drillForward();
    else
        m_RobotThread.drillBackward();
}

void ControlWindow::drillRotate()
{
    if(p_drillButton2->isChecked())
        m_RobotThread.drillRotate();
    else
        m_RobotThread.drillStop();
}


void ControlWindow::updatePoseDisplay(double x, double y, double theta)
{
    QString xPose, yPose, aPose;
    xPose.setNum(x);
    yPose.setNum(y);
    aPose.setNum(theta);

//    p_xDisplay->setText(xPose);
//    p_yDisplay->setText(yPose);
//    p_aDisplay->setText(aPose);
}//update the display.
}//end namespace


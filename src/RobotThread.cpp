#include "../include/RobotThread.h"
#include "nav_msgs/Odometry.h"
#include <iostream>
#include <random_numbers/random_numbers.h>

RobotThread::RobotThread(int argc, char** pArgv)
    :	m_Init_argc(argc),
        m_pInit_argv(pArgv)
{/** Constructor for the robot thread **/}

RobotThread::~RobotThread()
{

    if (ros::isStarted())
    {
        ros::shutdown();
        ros::waitForShutdown();
    }//end if

    m_pThread->wait();
}//end destructor

bool RobotThread::init()
{
    m_pThread = new QThread();
    this->moveToThread(m_pThread);

    connect(m_pThread, &QThread::started, this, &RobotThread::run);
    ros::init(m_Init_argc, m_pInit_argv, "gui_command");

    if (!ros::master::check())
        return false;//do not start without ros.

    ros::start();
    ros::Time::init();
    ros::NodeHandle nh;

    sim_velocity  = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 100);
    pub_key = nh.advertise<std_msgs::String>("/key", 100);
    pub_odom = nh.advertise<nav_msgs::Odometry>("odometry", 1000);

    sub_encoder = nh.subscribe("/encoder", 10, &RobotThread::encoderCallback, this);

    m_pThread->start();
    return true;
}//set up the thread

void RobotThread::poseCallback(const nav_msgs::Odometry & msg)
{
    QMutex * pMutex = new QMutex();

    pMutex->lock();
    m_xPos = msg.pose.pose.position.x;
    m_yPos = msg.pose.pose.position.y;
    m_aPos = msg.pose.pose.orientation.w;
    pMutex->unlock();

    delete pMutex;
    Q_EMIT newPose(m_xPos, m_yPos, m_aPos);
}//callback method to update the robot's position.

void RobotThread::encoderCallback(const std_msgs::Int32 & msg)
{
    QMutex * pMutex = new QMutex();

    nav_msgs::Odometry odometry;
    odometry.header.stamp = ros::Time::now();
    odometry.header.frame_id = "world";

    odometry.pose.pose.position.x = msg.data;
    odometry.pose.pose.position.y = 0;
    odometry.pose.pose.position.z = 0;
    odometry.pose.pose.orientation.x = 0;
    odometry.pose.pose.orientation.y = 0;
    odometry.pose.pose.orientation.z = 0;
    odometry.pose.pose.orientation.w = 1;
    odometry.twist.twist.linear.x = 0;
    odometry.twist.twist.linear.y = 0;
    odometry.twist.twist.linear.z = 0;

    pub_odom.publish(odometry);

    pMutex->lock();
    pMutex->unlock();

    delete pMutex;
}//callback method to update the robot's position.

void RobotThread::run()
{
    ros::Rate loop_rate(100);
    QMutex * pMutex;
    while (ros::ok())
    {
        pMutex = new QMutex();

        geometry_msgs::Twist cmd_msg;
        pMutex->lock();
        cmd_msg.linear.x = m_speed;
        cmd_msg.angular.z = m_angle;
        pMutex->unlock();

        if(flag == 1)
        {
            x = x + 0.001;
        }
        else if(flag == -1)
        {
            x = x - 0.001;
        }
        random_numbers::RandomNumberGenerator
        nav_msgs::Odometry odometry;
        odometry.header.stamp = ros::Time::now();
        odometry.header.frame_id = "world";
        odometry.pose.pose.position.x = x;
        odometry.pose.pose.position.y = 0;
        odometry.pose.pose.position.z = 0;
        odometry.pose.pose.orientation.x = 0;
        odometry.pose.pose.orientation.y = 0;
        odometry.pose.pose.orientation.z = 0;
        odometry.pose.pose.orientation.w = 1;
        odometry.twist.twist.linear.x = 0;
        odometry.twist.twist.linear.y = 0;
        odometry.twist.twist.linear.z = 0;

        pub_odom.publish(odometry);

        ros::spinOnce();
        loop_rate.sleep();
        delete pMutex;
    }//do ros things.
}

void RobotThread::SetSpeed(double speed, double angle)
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    m_speed = speed;
    m_angle = angle;
    pMutex->unlock();

    delete pMutex;
}//set the speed of the robot.

void RobotThread::moveForward()
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    std_msgs::String msg;
    msg.data = "moveForward";
    pub_key.publish(msg);
    pMutex->unlock();
    delete pMutex;

    flag = 1;
}

void RobotThread::moveBackward()
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    std_msgs::String msg;
    msg.data = "moveBackward";
    pub_key.publish(msg);
    pMutex->unlock();
    delete pMutex;

    flag = -1;
}

void RobotThread::moveStop()
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    std_msgs::String msg;
    msg.data = "moveStop";
    pub_key.publish(msg);
    pMutex->unlock();
    delete pMutex;

    flag = 0;
}

void RobotThread::drillForward()
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    std_msgs::String msg;
    msg.data = "drillForward";
    pub_key.publish(msg);
    pMutex->unlock();
    delete pMutex;
}
void RobotThread::drillBackward()
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    std_msgs::String msg;
    msg.data = "drillBackward";
    pub_key.publish(msg);
    pMutex->unlock();
    delete pMutex;
}
void RobotThread::drillRotate()
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    std_msgs::String msg;
    msg.data = "drillRotate";
    pub_key.publish(msg);
    pMutex->unlock();
    delete pMutex;
}
void RobotThread::drillStop()
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    std_msgs::String msg;
    msg.data = "drillStop";
    pub_key.publish(msg);
    pMutex->unlock();
    delete pMutex;
}

void RobotThread::armOpen()
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    std_msgs::String msg;
    msg.data = "armOpen";
    pub_key.publish(msg);
    pMutex->unlock();
    delete pMutex;
}

void RobotThread::armParallel()
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    std_msgs::String msg;
    msg.data = "armParallel";
    pub_key.publish(msg);
    pMutex->unlock();
    delete pMutex;
}

void RobotThread::armClose()
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    std_msgs::String msg;
    msg.data = "armClose";
    pub_key.publish(msg);
    pMutex->unlock();
    delete pMutex;
}

void RobotThread::armForward()
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    std_msgs::String msg;
    msg.data = "armForward";
    pub_key.publish(msg);
    pMutex->unlock();
    delete pMutex;
}

void RobotThread::armStop()
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    std_msgs::String msg;
    msg.data = "armStop";
    pub_key.publish(msg);
    pMutex->unlock();
    delete pMutex;
}

void RobotThread::armBackward()
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    std_msgs::String msg;
    msg.data = "armBackward";
    pub_key.publish(msg);
    pMutex->unlock();
    delete pMutex;
}

void RobotThread::turnLeft()
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    std_msgs::String msg;
    msg.data = "turnLeft";
    pub_key.publish(msg);
    pMutex->unlock();
    delete pMutex;
}

void RobotThread::turnRight()
{
    QMutex * pMutex = new QMutex();
    pMutex->lock();
    std_msgs::String msg;
    msg.data = "turnRight";
    pub_key.publish(msg);
    pMutex->unlock();
    delete pMutex;
}

double RobotThread::getXSpeed(){ return m_speed; }
double RobotThread::getASpeed(){ return m_angle; }

double RobotThread::getXPos(){ return m_xPos; }
double RobotThread::getYPos(){ return m_yPos; }
double RobotThread::getAPos(){ return m_aPos; }


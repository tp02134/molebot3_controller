#ifndef CONTROL_WINDOW_H
#define CONTROL_WINDOW_H
#include <QtWidgets>
#include <QPushButton>
#include <QString>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QTextStream>
#include <QLineEdit>
#include <QPalette>
#include <QIcon>
#include "RobotThread.h"

namespace server{
#define PI 3.14159265359

class ControlWindow : public QWidget
{
    Q_OBJECT

public:
    ControlWindow(int argc, char** argv, QWidget * parent = 0);

    Q_SLOT void updatePoseDisplay(double x, double y, double theta);
private:
    Q_SLOT void moveForward();
    Q_SLOT void moveBackward();
    Q_SLOT void turnLeft();
    Q_SLOT void turnRight();
    Q_SLOT void stop();
    Q_SLOT void close();
    Q_SLOT void drillExpand();
    Q_SLOT void drillRotate();
    Q_SLOT void armOpen();
    Q_SLOT void armParallel();
    Q_SLOT void armClose();
    Q_SLOT void armForward();
    Q_SLOT void armStop();
    Q_SLOT void armBackward();


    QPushButton *p_upButton;
    QPushButton *p_downButton;
    QPushButton *p_leftButton;
    QPushButton *p_rightButton;
    QPushButton *p_stopButton;
    QPushButton *p_quitButton;
    QPushButton *p_armButton1;
    QPushButton *p_armButton2;
    QPushButton *p_armButton3;
    QPushButton *p_armButton4;
    QPushButton *p_armButton5;
    QPushButton *p_armButton6;
    QPushButton *p_drillButton1;
    QPushButton *p_drillButton2;

    QVBoxLayout *drillLayout;
    QHBoxLayout *subdrillLayout1;
    QHBoxLayout *subdrillLayout2;
    QHBoxLayout *subdrillLayout3;
    QHBoxLayout *subdrillLayout4;
    QHBoxLayout *subdrillLayout5;
    QHBoxLayout *subdrillLayout6;
    QHBoxLayout *subdrillLayout7;

    QVBoxLayout *forelimbLayout;
    QHBoxLayout *subforelimbLayout1;
    QHBoxLayout *subforelimbLayout2;
    QVBoxLayout *subforelimbLayout3;
    QVBoxLayout *subforelimbLayout4;
    QHBoxLayout *subforelimbLayout5;

    QVBoxLayout *movementLayout;
    QHBoxLayout *submovementLayout1;
    QHBoxLayout *submovementLayout2;
    QHBoxLayout *submovementLayout3;
    QHBoxLayout *submovementLayout4;
    QHBoxLayout *submovementLayout5;
    QHBoxLayout *submovementLayout6;
    QHBoxLayout *submovementLayout7;

    QLabel *p_nameLabel1;
    QLabel *p_nameLabel2;
    QLabel *p_nameLabel3;

    QFrame *p_line1;
    QFrame *p_line2;
    QFrame *p_line3;
    QFrame *p_line4;
    QFrame *p_line5;

    QLineEdit *p_display1;
    QLineEdit *p_display2;

    QHBoxLayout *mainLayout;
    QPushButton *closeButton;

    RobotThread m_RobotThread;

    int num = 0;
};//end class ControlWindow
}//end namespace
#endif


#ifndef ___ROBOTTHREAD_H___
#define ___ROBOTTHREAD_H___

#include <QtCore>
#include <QThread>
#include <QStringList>
#include <stdlib.h>
#include <QMutex>
#include <iostream>
#include "assert.h"

#include <ros/ros.h>
#include <ros/network.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Int32.h>
#include <std_msgs/String.h>

class RobotThread : public QObject {
	Q_OBJECT
public:
    RobotThread(int argc, char **pArgv);
    virtual ~RobotThread();

    double getXPos();
    double getXSpeed();
    double getASpeed();
    double getYPos();
    double getAPos();

    double x = 0.0;
    int flag = 0;

    bool init();

    void poseCallback(const nav_msgs::Odometry & msg);
    void encoderCallback(const std_msgs::Int32 & msg);

    void moveForward();
    void moveStop();
    void moveBackward();
    void turnLeft();
    void turnRight();

    void drillForward();
    void drillBackward();
    void drillRotate();
    void drillStop();

    void armOpen();
    void armParallel();
    void armClose();
    void armForward();
    void armStop();
    void armBackward();

    void SetSpeed(double speed, double angle);
    void setPose(QList<double> to_set);

    Q_SLOT void run();

    Q_SIGNAL void newPose(double,double,double);

private:
    int m_Init_argc;
    char** m_pInit_argv;
    const char * m_topic;

    double m_speed;
    double m_angle;

    double m_xPos;
    double m_yPos;
    double m_aPos;

    double m_maxRange;
    double m_minRange;

    QThread * m_pThread;

    ros::Publisher sim_velocity;
    ros::Publisher pub_key;
    ros::Publisher pub_odom;

    ros::Subscriber pose_listener;
    ros::Subscriber sub_encoder;
};
#endif

